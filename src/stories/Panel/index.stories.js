import React from "react";
import Panel from ".";

import styles from "./index.module.scss";

export default {
  title: "INUI/Panel",
  component: Panel,
};

const Text = (props) => <div {...props} />;

export const Default = () => (
  <Panel style={{ marginTop: 100 }}>
    <Text>
      Color: <strong>White</strong>
    </Text>
    <Text>
      Border Radius: <strong>{styles.borderRadius}</strong>
    </Text>
    <Text>
      Shadow: <strong>Panel-Shadow</strong>
    </Text>
  </Panel>
);
