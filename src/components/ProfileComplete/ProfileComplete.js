import React from 'react';
// import './profile.css';
import {FaPlus} from 'react-icons/fa';
import 'react-circular-progressbar/dist/styles.css';
import '../../components/ProfileComplete/profile.scss'
import 'bootstrap/dist/css/bootstrap.min.css';
import {
     CircularProgressbar,
     buildStyles
   } from "react-circular-progressbar";
const ProfileComplete = ({ children }) => {
  return (
    <div className="container-fluid">
    <div className="panel row px-2 pt-4 m-auto"  >
      <div className="heading-discription col-md-7">
            <p className="heading font-weight-bold d-none d-md-block d-lg-block text-left"> Profil-Vollständigkeit</p>
            <p className="desc text-center text-md-left text-lg-left py-3 py-md-0 py-lg-0">Um deine Chancen für einen Match auf mögliche Jobangebote zu erhöhen, benötigen wir mehr Informationen zu dir. Erwäge die folgenden Punkte, um größtmöglichen Erfolg zu haben.</p>
     </div>
      <div className="pannel-icons col-md-3" >
        <div>
           <div><span className="icon"><FaPlus  size=".8rem"/></span><p className="iconlist text-left">Dein Werdegang</p></div>
           <div><span className="icon"><FaPlus  size=".8rem"/></span><p className="iconlist text-left">Weitere Skills</p></div>
           <div><span className="icon"><FaPlus  size=".8rem"/></span><p className="iconlist text-left">Branchenerfahrung</p></div>
       </div> 
     </div>
      <div className="pannel-guage col-12 col-lg-2  col-md-2 order-first order-md-last order-lg-last mb-2" >

      <p className="heading-media font-weight-bold d-block  d-md-none d-lg-none pb-2 text-left"> Profil-Vollständigkeit</p>
      
     <div className="flex-42"> <CircularProgressbar
        value="78"
        text="78%"
        strokeWidth={10}
        styles={buildStyles({
          textColor: "red",
          pathColor: "#ED8838",
          trailColor: "#F5F5F5",
          strokeLinecap: "butt"
        })}
      />
      </div>
      </div>
     </div>
        </div>
  )
}

export default ProfileComplete;